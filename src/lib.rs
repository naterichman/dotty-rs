use pyo3::{prelude::*, types::PyDict, exceptions::PyValueError};

mod dotty;
use dotty::{Dotty, DottyValue};

#[pyclass]
struct DottyRS {
    inner: Dotty,
}

#[pymethods]
impl DottyRS {
    #[new]
    fn __new__(dict: Py<PyDict>) -> DottyRS {
        let dict_string = dict.to_string().replace("'", "\"");
        let json = serde_json::from_str(dict_string.as_str());
        //let json: Result<Value, _> = serde_json::from_str(dict.to_string().as_str());
        println!("{:?}", json);
        DottyRS { 
            inner: Dotty::new(json.unwrap())
        }
    }

    fn __str__(&self) -> PyResult<String> {
        Ok(self.inner.to_string())
    }

    fn __getitem__(&self, key: String) -> PyResult<PyObject> {
        let val = self.inner.get(key).
            map_err(|err| PyValueError::new_err(err))?;
        Ok(Python::with_gil(|py|{
            DottyValue(val.into_owned()).into_py(py)
        }))
    }

}


/// A Python module implemented in Rust.
#[pymodule]
fn dotty_rs(_py: Python, m: &PyModule) -> PyResult<()> {
    m.add_class::<DottyRS>()?;
    Ok(())
}