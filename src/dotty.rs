use pyo3::{Python, PyResult, PyObject, types::PyDict, IntoPy, ToPyObject, exceptions::PyValueError};
use serde_json::{Map, Value};
use std::{collections::HashMap, ops::Range, borrow::Cow};


pub struct DottyValue(pub Value);

fn convert_to_py(py: Python, value: &Value) -> PyResult<PyObject> {
    match value {
        Value::Null => Ok(py.None()),
        Value::Bool(bool_) => Ok(bool_.to_object(py)),
        Value::Number(num) => {
            if let Some(i) = num.as_i64() {
                Ok(i.to_object(py)) // Convert integer to Python int
            } else if let Some(f) = num.as_f64() {
                Ok(f.to_object(py)) // Convert float to Python float
            } else {
                // Handle other numeric types as needed
                Err(PyValueError::new_err("Unsupported numeric type"))
            }
        },
        Value::String(str_) => Ok(str_.to_object(py)),
        Value::Array(arr) => {
            let mut list: Vec<PyObject> = Vec::with_capacity(arr.len());
            for el in arr.iter() {
                let res = convert_to_py(py, el)?;
                list.push(res);
            }
           Ok(list.to_object(py))
        } 
        Value::Object(obj) => {
            let py_dict = PyDict::new(py);
            for (key, value) in obj.iter() {
                let py_key = key.to_object(py);
                let py_value = convert_to_py(py, value)?;
                py_dict.set_item(py_key, py_value)?;
            }
            Ok(py_dict.to_object(py)) // Convert object to Python dict 
        }
    }
}


impl IntoPy<PyObject> for DottyValue {
    fn into_py(self, py: Python<'_>) -> PyObject {
        convert_to_py(py, &self.0).unwrap()
    }
}

enum QueryComponent {
    Field(String),
    Index(usize),
    Range(Range<usize>),
    All
}

pub struct Query(Vec<QueryComponent>);

//impl Query {
//    pub fn from(query: String, separator: &String) -> Result<Self, String> {
//        let components: Vec<QueryComponent> = query.split(separator)
//            .map(|component| {
//                if component == String::from("*") {
//                    QueryComponent::All
//                 }
//                 let idx = parse_index(component).and_then
//             })
//             .collect();

//     }
// }


pub struct Dotty {
    data: Value,
    get_cache: HashMap<String, serde_json::Value>,
    separator: String,
}
impl Dotty {
    pub fn new(data: Value) -> Self {
        Self {
            data: data,
            get_cache: HashMap::<String, Value>::new(),
            separator: String::from('.'),
        }
    }

    pub fn default() -> Self {
        Self {
            data: Value::Object(Map::new()),
            get_cache: HashMap::<String, Value>::new(),
            separator: String::from('.'),
        }
    }

    fn split(&self, key: String) -> Vec<String> {
        key.split(&self.separator).map(String::from).collect()
    }   

    pub fn get(&self, key: String) -> Result<Cow::<'_, Value>, String> {
        println!("{:?}", key);
        let keys = self.split(key);
        get_value(keys, Cow::Borrowed(&self.data))
        
    }

    pub fn to_string(&self) -> String {
        let json = serde_json::to_string_pretty(&self.data.to_string());
        json.unwrap()
    }

}

fn parse_index(idx: &str) -> Option<usize> {
    idx.parse().ok()
}

fn parse_range(range: &str) -> Option<Range<usize>> {
    let parts: Vec<&str> = range.split(":").collect();
    if parts.len() != 2 { return None }

    let start = parts[0].parse::<usize>().ok();
    let end = parts[1].parse::<usize>().ok();

    Some(start?..end?)
}


fn get_value<'a>(mut keys: Vec<String>, data: Cow<'a, Value>) -> Result<Cow<'a, Value>, String> {
    match data {
        Cow::Borrowed(val) => {
            match val {
                Value::Array(arr) => {
                    // First handle simple index
                    let key = keys.remove(0);
                    get_from_array(&key, keys.clone(), Cow::Borrowed(arr))
                },
                Value::Object(map) => {
                    let key = keys.remove(0);
                    if let Some(sub_dict) = map.get(&key) {
                        if keys.len() > 0 {
                            let res = get_value(keys.clone(), Cow::Borrowed(sub_dict))?;
                            Ok(res)
                        } else {
                            return Ok(Cow::Borrowed(sub_dict));
                        }
                    } else {
                        Ok(Cow::Owned(Value::Null))
                    }
                },
                _ => Ok(Cow::Borrowed(&val))
            }
        }
        Cow::Owned(val) => {
            match val {
                Value::Array(arr) => {
                    // First handle simple index
                    let key = keys.remove(0);
                    get_from_array(&key, keys.clone(), Cow::Owned(arr))
                },
                Value::Object(map) => {
                    let key = keys.remove(0);
                    if let Some(sub_dict) = map.get(&key) {
                        if keys.len() > 0 {
                            let res = get_value(keys.clone(), Cow::Owned(sub_dict.clone()))?;
                            Ok(res)
                        } else {
                            return Ok(Cow::Owned(sub_dict.clone()));
                        }
                    } else {
                        Ok(Cow::Owned(Value::Null))
                    }
                },
                _ => Ok(Cow::Owned(val))
            }
        }
    }
}


fn get_from_array<'a>(key: &str, keys: Vec<String>, data: Cow<'a, Vec<Value>>) -> Result<Cow<'a, Value>, String> {
    match data {
        Cow::Borrowed(val) => {
                let idx_res = parse_index(&key);
                if idx_res.is_some() {
                    let idx = idx_res.unwrap();
                    if  idx < val.len() {
                        if keys.len() > 0 {
                            return Ok(get_value(keys.clone(), Cow::Borrowed(&val[idx]))?);
                        } else {
                            return Ok(Cow::Borrowed(&val[idx]));
                        }
                    } else {
                        return Err(String::from(format!("Index out of range: {}", idx)));
                    }
                }

                let range_res = parse_range(key);
                if range_res.is_some() {
                    let idx_range = range_res.unwrap();
                    let mut res = Vec::<Value>::new();
                    for i in idx_range {
                        if i < val.len() {
                            //println!("{:?}\n\t {:?}\n\t {:?}", val, &val[i], &val[i][0]);
                            let it = get_value(keys.clone() ,Cow::Borrowed(&val[i]))?;
                            res.push(it.into_owned());
                        }
                    }
                    return Ok(Cow::Owned(Value::Array(res)));
                }
                Err(String::from(format!("Could not parse index {}", key)))
        } 
        Cow::Owned(val) => {
                let idx_res = parse_index(&key);
                if idx_res.is_some() {
                    let idx = idx_res.unwrap();
                    if  idx < val.len() {
                        if keys.len() > 0 {
                            return Ok(get_value(keys, Cow::Owned((&val[idx]).clone()))?);
                        } else {
                            return Ok(Cow::Owned((&val[idx]).clone()));
                        }
                    } else {
                        return Err(String::from(format!("Index out of range: {}", idx)));
                    }
                }

                let range_res = parse_range(key);
                if range_res.is_some() {
                    let idx_range = range_res.unwrap();
                    let mut res = Vec::<Value>::new();
                    for i in idx_range {
                        if i < val.len() {
                            println!("{:?}", &val[i]);
                            let it = get_value(keys.clone() ,Cow::Borrowed(&val[i]))?;
                            res.push(it.into_owned());
                        }
                    }
                    return Ok(Cow::Owned(Value::Array(res)));
                }
                Err(String::from(format!("Could not parse index {}", key)))
        }
    }
}


#[cfg(test)]
mod test {
    use serde_json::{Value, from_str};
    use super::*;
    #[test]
    fn test_init() {
        let json_str = r#"
            {
                "test": {
                    "test2": [
                        "1",
                        "2"
                    ]
                } 
            }
        "#;
        let json = from_str(json_str).unwrap();
        let dotty = Dotty::new(json);
        assert_eq!(
            dotty.get(String::from("test.test2.0")).unwrap().into_owned(),
            Value::String(String::from("1"))
        );
        assert_eq!(
            dotty.get(String::from("test.test2.0:2")).unwrap().into_owned(),
            Value::Array(vec![
                Value::String(String::from("1")),
                Value::String(String::from("2")),
            ])
        );
    }

    #[test]
    fn test_nested_list() {
        let json_str = r#"
            {
                "test": {
                    "test2": [
                        {
                            "a": [1, 2]
                        },
                        {
                            "a": [3, 4]
                        }
                    ]
                }
            }
        "#;

        let json = from_str(json_str).unwrap();
        let dotty = Dotty::new(json);
        assert_eq!(
            dotty.get(String::from("test.test2.0:2.a.0:2")).unwrap().into_owned(),
            Value::Array(vec![
                Value::Array(vec![Value::Number(1), Value::Number(2)]),
                Value::Array(vec![Value::Number(3), Value::Number(4)]),
            ])
        );
    }
}